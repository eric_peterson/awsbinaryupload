var http = require('http');
var fs = require('fs');

http.createServer(function(request,response){

    response.writeHead(200);
    var destinationFile = fs.createWriteStream("image.png");
    request.pipe(destinationFile);

    var fileSize = request.headers['content-length'];
    var uploadedBytes = 0 ;

    request.on('data',function(d){

        uploadedBytes += d.length;
        if( fileSize > 0 ) {
            var p = (uploadedBytes/fileSize) * 100;
            response.write("Uploading " + parseInt(p)+ " %\n");
        } else {
            response.write("Uploaded " + uploadedBytes + "\n");
        }
    });

    request.on('end',function(){
        response.end("File Upload Complete");
    });

}).listen(8080,function(){ console.log("Server started"); });
